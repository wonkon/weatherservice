package com.wonkon.openweatherapi.transformer;

import com.wonkon.openweatherapi.model.Weather;
import com.wonkon.weatherservice.WeatherDTO;

public class WeatherTransformer {

    public static WeatherDTO toDTO(Weather weather) {
        if (weather == null) {
            return null;
        }
        WeatherDTO weatherDTO = new WeatherDTO();
        weatherDTO.setTemperature(weather.getMain().getTemperature());
        weatherDTO.setFeelsLikeTemperature(weather.getMain().getTemperature());
        weatherDTO.setHumidity(weather.getMain().getHumidity());
        weatherDTO.setPressure(weather.getMain().getPressure());
        weatherDTO.setWindSpeed(weather.getWind().getSpeed());
        weatherDTO.setWindDirection(weather.getWind().getDirection());
        weatherDTO.setCountry(weather.getSys().getCountry());
        weatherDTO.setCity(weather.getName());
        return weatherDTO;
    }
}
