package com.wonkon.openweatherapi.service;

import com.wonkon.weatherservice.ExternalWeatherApiService;
import com.wonkon.weatherservice.WeatherDTO;
import com.wonkon.openweatherapi.transformer.WeatherTransformer;
import com.wonkon.openweatherapi.executor.OpenWeatherApiExecutor;
import com.wonkon.openweatherapi.model.Weather;

import java.io.IOException;

public class OpenWeatherApiService implements ExternalWeatherApiService {
    private final OpenWeatherApiExecutor openWeatherApiExecutor;

    public OpenWeatherApiService(OpenWeatherApiExecutor openWeatherApiExecutor) {
        this.openWeatherApiExecutor = openWeatherApiExecutor;
    }

    @Override
    public WeatherDTO fetchWeatherByCityName(String cityName) {
        Weather weather = null;
        try {
            weather = openWeatherApiExecutor.fetchWeatherByCityName(cityName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return WeatherTransformer.toDTO(weather);
    }
}
