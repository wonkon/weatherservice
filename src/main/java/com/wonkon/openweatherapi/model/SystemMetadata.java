package com.wonkon.openweatherapi.model;

public class SystemMetadata {
    private String country;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "SystemMetadata{" +
                "country='" + country + '\'' +
                '}';
    }
}
