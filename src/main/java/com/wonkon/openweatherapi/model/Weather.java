package com.wonkon.openweatherapi.model;

public class Weather {
    private String name;
    private SystemMetadata sys;
    private Main main;
    private Wind wind;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SystemMetadata getSys() {
        return sys;
    }

    public void setSys(SystemMetadata sys) {
        this.sys = sys;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "name='" + name + '\'' +
                ", sys=" + sys +
                ", main=" + main +
                ", wind=" + wind +
                '}';
    }
}
