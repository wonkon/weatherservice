package com.wonkon;

import com.wonkon.http.ExtHttpClient;
import com.wonkon.openweatherapi.executor.OpenWeatherApiExecutor;
import com.wonkon.openweatherapi.service.OpenWeatherApiService;
import com.wonkon.weatherservice.WeatherRepository;
import com.wonkon.weatherservice.WeatherService;
import com.wonkon.weatherstackapi.service.WeatherStackService;
import org.h2.tools.Server;
import com.wonkon.weatherstackapi.executor.WeatherStackExecutor;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;

public class WeatherServiceApplication {

    public static void main(String[] args) throws IOException, SQLException {
        Server server = Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8083");
        server.start();
        ExtHttpClient extHttpClient = new ExtHttpClient();
        OpenWeatherApiExecutor openWeatherApiExecutor = new OpenWeatherApiExecutor(extHttpClient);
        OpenWeatherApiService openWeatherApiService = new OpenWeatherApiService(openWeatherApiExecutor);
        WeatherStackExecutor weatherStackExecutor = new WeatherStackExecutor(extHttpClient);
        WeatherStackService weatherStackService = new WeatherStackService(weatherStackExecutor);
        WeatherRepository weatherRepository = new WeatherRepository();
        WeatherService weatherService = new WeatherService(weatherRepository, Arrays.asList(weatherStackService, openWeatherApiService));
        //odczyt 21:00
        weatherService.fetchWeatherByCityName("Szczecin");
        //odczyt 21:30
        weatherService.fetchWeatherByCityName("Szczecin");
        //odczyt 22:00
        weatherService.fetchWeatherByCityName("Szczecin");
        System.out.println("Average: " + weatherService.getAverageWeatherParams("Szczecin"));
    }
}
