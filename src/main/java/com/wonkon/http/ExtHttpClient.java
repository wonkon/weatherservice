package com.wonkon.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ExtHttpClient {

    private final Gson gson;

    public ExtHttpClient() {
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

    public <T> T get(String url, Class<T> classOfT) {
        T object = null;
        try {
            URL urlAddress = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlAddress.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((InputStream) httpURLConnection.getContent()));
            object = gson.fromJson(bufferedReader, classOfT);
            httpURLConnection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return object;
    }
}
