package com.wonkon.weatherstackapi.service;

import com.wonkon.weatherstackapi.model.Weather;
import com.wonkon.weatherservice.ExternalWeatherApiService;
import com.wonkon.weatherservice.WeatherDTO;
import com.wonkon.weatherstackapi.executor.WeatherStackExecutor;
import com.wonkon.weatherstackapi.transformer.WeatherTransformer;

public class WeatherStackService implements ExternalWeatherApiService {

    private final WeatherStackExecutor weatherStackExecutor;

    public WeatherStackService(WeatherStackExecutor weatherStackExecutor) {
        this.weatherStackExecutor = weatherStackExecutor;
    }

    @Override
    public WeatherDTO fetchWeatherByCityName(String city) {
        Weather weather = weatherStackExecutor.fetchWeatherByCityName(city);
        return WeatherTransformer.toDTO(weather);
    }
}
