package com.wonkon.weatherstackapi.model;

public class Weather {
    private CurrentWeather current;
    private Location location;

    public CurrentWeather getCurrent() {
        return current;
    }

    public void setCurrent(CurrentWeather current) {
        this.current = current;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "current=" + current +
                ", location=" + location +
                '}';
    }
}
