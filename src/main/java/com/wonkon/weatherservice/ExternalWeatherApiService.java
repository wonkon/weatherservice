package com.wonkon.weatherservice;

public interface ExternalWeatherApiService {
    WeatherDTO fetchWeatherByCityName(String cityName);
}
