package com.wonkon.weatherservice;

import javax.persistence.*;
import java.util.List;

public class WeatherRepository {

    private final EntityManager entityManager;

    public WeatherRepository() {
        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("weather_service_entity_manager");
        entityManager = entityManagerFactory.createEntityManager();
    }

    public void add(WeatherDTO weatherDTO) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.persist(weatherDTO);
            transaction.commit();
        } catch (IllegalArgumentException e) {
            transaction.rollback();
        }
    }

    public List<WeatherDTO> findByCity(String city) {
        Query query = entityManager.createQuery("FROM WeatherDTO weather WHERE weather.city=(:city)");
        query.setParameter("city", city);
        return query.getResultList();
    }
}
