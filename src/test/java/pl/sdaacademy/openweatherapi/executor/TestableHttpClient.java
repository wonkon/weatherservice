package pl.sdaacademy.openweatherapi.executor;

import com.google.gson.Gson;
import com.wonkon.http.ExtHttpClient;

public class TestableHttpClient extends ExtHttpClient {

    @Override
    public <T> T get(String url, Class<T> classOfT) {
        Gson gson = new Gson();
        return gson.fromJson("{\n" +
                "  \"coord\": {\n" +
                "    \"lon\": 21.0118,\n" +
                "    \"lat\": 52.2298\n" +
                "  },\n" +
                "  \"weather\": [\n" +
                "    {\n" +
                "      \"id\": 802,\n" +
                "      \"main\": \"Clouds\",\n" +
                "      \"description\": \"scattered clouds\",\n" +
                "      \"icon\": \"03d\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"base\": \"stations\",\n" +
                "  \"main\": {\n" +
                "    \"temp\": 14.7,\n" +
                "    \"feels_like\": 13.82,\n" +
                "    \"temp_min\": 12.9,\n" +
                "    \"temp_max\": 15.92,\n" +
                "    \"pressure\": 1015,\n" +
                "    \"humidity\": 61\n" +
                "  },\n" +
                "  \"visibility\": 10000,\n" +
                "  \"wind\": {\n" +
                "    \"speed\": 3.6,\n" +
                "    \"deg\": 350\n" +
                "  },\n" +
                "  \"clouds\": {\n" +
                "    \"all\": 40\n" +
                "  },\n" +
                "  \"dt\": 1622279342,\n" +
                "  \"sys\": {\n" +
                "    \"type\": 2,\n" +
                "    \"id\": 2017407,\n" +
                "    \"country\": \"PL\",\n" +
                "    \"sunrise\": 1622255009,\n" +
                "    \"sunset\": 1622313816\n" +
                "  },\n" +
                "  \"timezone\": 7200,\n" +
                "  \"id\": 756135,\n" +
                "  \"name\": \"Warsaw\",\n" +
                "  \"cod\": 200\n" +
                "}", classOfT);
    }
}
