package pl.sdaacademy.openweatherapi.executor;

import com.wonkon.http.ExtHttpClient;
import com.wonkon.openweatherapi.executor.OpenWeatherApiExecutor;
import com.wonkon.openweatherapi.model.Weather;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class OpenWeatherApiExecutorTest {


    @Test
    void when_something_then_something_should_happen() throws IOException {
        //given
        ExtHttpClient httpClient = new TestableHttpClient();
        OpenWeatherApiExecutor openWeatherApiExecutor = new OpenWeatherApiExecutor(httpClient);

        //when
        Weather weather = openWeatherApiExecutor.fetchWeatherByCityName("Warsaw");

        //then
        assertEquals("PL", weather.getSys().getCountry());
        assertEquals(14.7, weather.getMain().getTemperature(), 0.1);
    }

}