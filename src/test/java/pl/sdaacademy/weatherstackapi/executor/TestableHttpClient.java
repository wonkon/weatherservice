package pl.sdaacademy.weatherstackapi.executor;

import com.google.gson.Gson;
import com.wonkon.http.ExtHttpClient;

public class TestableHttpClient extends ExtHttpClient {

    @Override
    public <T> T get(String url, Class<T> classOfT) {
        Gson gson = new Gson();
        return gson.fromJson("{\n" +
                "  \"request\": {\n" +
                "    \"type\": \"City\",\n" +
                "    \"query\": \"Warsaw, Poland\",\n" +
                "    \"language\": \"en\",\n" +
                "    \"unit\": \"m\"\n" +
                "  },\n" +
                "  \"location\": {\n" +
                "    \"name\": \"Warsaw\",\n" +
                "    \"country\": \"Poland\",\n" +
                "    \"region\": \"\",\n" +
                "    \"lat\": \"52.250\",\n" +
                "    \"lon\": \"21.000\",\n" +
                "    \"timezone_id\": \"Europe/Warsaw\",\n" +
                "    \"localtime\": \"2021-05-30 10:44\",\n" +
                "    \"localtime_epoch\": 1622371440,\n" +
                "    \"utc_offset\": \"2.0\"\n" +
                "  },\n" +
                "  \"current\": {\n" +
                "    \"observation_time\": \"08:44 AM\",\n" +
                "    \"temperature\": 15,\n" +
                "    \"weather_code\": 116,\n" +
                "    \"weather_icons\": [\n" +
                "      \"https://assets.weatherstack.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png\"\n" +
                "    ],\n" +
                "    \"weather_descriptions\": [\n" +
                "      \"Partly cloudy\"\n" +
                "    ],\n" +
                "    \"wind_speed\": 20,\n" +
                "    \"wind_degree\": 360,\n" +
                "    \"wind_dir\": \"N\",\n" +
                "    \"pressure\": 1020,\n" +
                "    \"precip\": 0.4,\n" +
                "    \"humidity\": 63,\n" +
                "    \"cloudcover\": 25,\n" +
                "    \"feelslike\": 14,\n" +
                "    \"uv_index\": 3,\n" +
                "    \"visibility\": 10,\n" +
                "    \"is_day\": \"yes\"\n" +
                "  }\n" +
                "}", classOfT);
    }
}
