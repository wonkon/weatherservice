package pl.sdaacademy.weatherstackapi.executor;

import org.junit.jupiter.api.Test;
import com.wonkon.http.ExtHttpClient;
import com.wonkon.weatherstackapi.executor.WeatherStackExecutor;
import com.wonkon.weatherstackapi.model.Weather;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class WeatherStackExecutorTest {

    @Test
    void when_fetch_weather_by_city_name_then_weather_object_should_be_returned() throws IOException {
        //given
        ExtHttpClient testableHttpClient = new TestableHttpClient();
        WeatherStackExecutor weatherStackExecutor = new WeatherStackExecutor(testableHttpClient);

        //when
        Weather weather = weatherStackExecutor.fetchWeatherByCityName("Warsaw");

        //then
        assertEquals(15, weather.getCurrent().getTemperature());
    }
}